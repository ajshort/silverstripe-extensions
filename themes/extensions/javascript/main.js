jQuery(function($) {
	$("#filter-maintainers").on("keyup", function() {
		var term = this.value.toLowerCase();

		$(".maintainers-list li").each(function() {
			$(this).toggle($(this).text().toLowerCase().indexOf(term) != -1);
		});
	});

	$(".readme .toggle").on("click", function() {
		$(this).parents(".readme").toggleClass("collapsed");
		return false;
	})
});
