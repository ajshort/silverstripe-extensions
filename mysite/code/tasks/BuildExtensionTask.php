<?php
/**
 * Builds a single extension, using metrics to get the one that should be built first.
 */
class BuildExtensionTask extends BuildTask {

	protected $title = 'Build Extension';

	protected $description = 'Builds a single extension';

	/**
	 * @var ExtensionBuilderService
	 */
	private $service;

	public function __construct(ExtensionBuilderService $service) {
		$this->service = $service;
	}

	public function run($request) {
		// Get the most popular extension which has not yet been build.
		$ext = ExtensionPackage::get()->where('"BuiltAt" IS NULL')->sort('Downloads', 'DESC')->first();

		if (!$ext) {
			// TODO
		}

		$start = time();
		echo "Building extension $ext->Name...\n";

		$this->service->build($ext);

		$time = time() - $start;
		echo "Done, took $time seconds\n";
	}

}
