<?php
/**
 * Runs the extension updater.
 */
class UpdateExtensionsTask extends BuildTask {

	protected $title = 'Update Extensions';

	protected $description = 'Updates extension packages from Packagist';

	/**
	 * @var ExtensionUpdaterService
	 */
	private $service;

	public function __construct(ExtensionUpdaterService $service) {
		$this->service = $service;
	}

	public function run($request) {
		$this->service->update();
	}

}
