<?php

use Composer\Downloader\DownloadManager;
use Composer\Package\PackageInterface;
use dflydev\markdown\MarkdownParser;

/**
 * Checks out a module and builds information about it.
 */
class ExtensionBuilderService {

	const DOWNLOADS_DIR = 'extensions';

	private $composer;

	public function __construct(ComposerService $composer) {
		$this->composer = $composer;
	}

	public function build(ExtensionPackage $package) {
		$manager = $this->composer->getComposer()->getDownloadManager();
		$packages = $this->composer->getGroupedPackage($package->Name);

		if (!$packages) {
			throw new Exception('Could not find the matching Composer packages');
		}

		// Extract the README from the most recent version.
		$latest = $package->Versions()->filter('IsDevelopment', true)->first();

		foreach ($packages as $matching) {
			if ($matching->getVersion() != $latest->Version) {
				continue;
			}

			$path = $this->download($matching, $manager);
			$package->Readme = $this->buildReadme($path);
			$package->write();

			break;
		}

		$package->BuiltAt = time();
		$package->write();
	}

	protected function download(PackageInterface $package, DownloadManager $manager) {
		$path = implode('/', array(
			TEMP_FOLDER, self::DOWNLOADS_DIR, $package->getUniqueName()
		));
		$manager->download($package, $path, true);

		return $path;
	}

	private function buildReadme($path) {
		$names = array(
			'README.md',
			'README.markdown',
			'README.mdown'
		);

		foreach ($names as $name) {
			$names[] = strtolower($name);
		}

		foreach ($names as $name) {
			$full = $path . '/' . $name;

			if (file_exists($full)) {
				$parser = new MarkdownParser();
				return $parser->transformMarkdown(file_get_contents($full));
			}
		}

		return '';
	}

}
