<?php

use Composer\Package\AliasPackage;
use Composer\Package\CompletePackage;
use Composer\Repository\RepositoryInterface;
use Guzzle\Http\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;

/**
 * Handles getting information from Packagist and storing it in the local database.
 */
class ExtensionUpdaterService {

	private $composer;
	private $client;

	public function __construct(ComposerService $composer) {
		$this->composer = $composer;
		$this->client = new Client('http://packagist.org/packages');
	}

	public function update() {
		foreach ($this->composer->getGroupedPackages() as $package) {
			$this->updateRepository($package);
		}
	}

	protected function updateRepository(RepositoryInterface $repository) {
		$packages  = $repository->getPackages();
		$name = $packages[0]->getName();
		$extension = ExtensionPackage::get()->filter('Name', $name)->first();

		if (!$extension) {
			$extension = new ExtensionPackage();
			$extension->Name = $name;
			$extension->write();
		}

		usort($packages, function ($a, $b) {
			return version_compare($a->getVersion(), $b->getVersion());
		});

		foreach ($packages as $package) {
			if ($package instanceof AliasPackage) {
				continue;
			}

			$this->updateVersion($extension, $package);
		}

		$this->updateExtension($extension);
	}

	private function updateExtension(ExtensionPackage $extension) {
		try {
			$request = $this->client->get($extension->Name . '.json');
			$data = $request->send()->json();

			$extension->Type = $data['package']['type'];
			$extension->Description = $data['package']['description'];
			$extension->Repository = $data['package']['repository'];

			if(isset($data['package']['downloads']['total']) && is_int($data['package']['downloads']['total'])) {
				$extension->Downloads =  $data['package']['downloads']['total'];
			}
		} catch(ClientErrorResponseException $e) {}

		$extension->UpdatedAt = time();
		$extension->write();
	}

	private function updateVersion(ExtensionPackage $extension, CompletePackage $package) {
		$version = $extension->getVersion($package->getVersion()) ?: new ExtensionVersion();

		// If the version is a tag, and the version already exists, then mark it as updated and
		// move on.
		if ($version->isInDB() && !$package->isDev()) {
			$extension->UpdatedAt = time();
			$extension->write();

			return;
		}

		DB::getConn()->transactionStart();

		// Set the version details.
		$version->Name = $package->getName();
		$version->Type = $package->getType();
		$version->Description = $package->getDescription();

		$version->Version = $package->getVersion();
		$version->PrettyVersion = $package->getPrettyVersion();
		$version->VersionAlias = $package->getAlias();
		$version->PrettyVersionAlias = $package->getPrettyAlias();
		$version->IsDevelopment = $package->isDev();

		$version->Homepage = $package->getHomepage();
		$version->License  = $package->getLicense();

		if ($package->getSourceType()) {
			$version->Source = array(
				'type'      => $package->getSourceType(),
				'url'       => $package->getSourceUrl(),
				'reference' => $package->getSourceReference()
			);
		} else {
			$version->Source = array();
		}

		if ($package->getDistType()) {
			$version->Source = array(
				'type'      => $package->getDistType(),
				'url'       => $package->getDistUrl(),
				'reference' => $package->getDistReference(),
				'checksum'  => $package->getDistSha1Checksum()
			);
		} else {
			$version->Dist = array();
		}

		$version->Support = $package->getSupport();
		$version->Extra = $package->getExtra();

		$tags = $extension->Tags->getValue() ?: array();

		if ($keywords = $package->getKeywords()) foreach ($keywords as $keyword) {
			if (!in_array($keyword, $tags)) {
				$tags[] = $keyword;
			}
		}

		$extension->Tags->setValue($tags);

		$released = $package->getReleaseDate()->getTimestamp();
		$version->ReleasedAt = $released;
		$version->UpdatedAt = time();

		// Set the original package release date to the earliest version's release date.
		if (!$extension->ReleasedAt || strtotime($extension->ReleasedAt) > $released) {
			$extension->ReleasedAt = $released;
		}

		$extension->Versions()->add($version);

		$this->updateMaintainers($version, $package);

		DB::getConn()->transactionEnd();
	}

	private function updateMaintainers(ExtensionVersion $version, CompletePackage $package) {
		if ($package->getAuthors()) foreach($package->getAuthors() as $author) {
			$maintainer = null;

			if (empty($author['name']) && empty($author['email'])) {
				continue;
			}

			if (!empty($author['email'])) {
				$maintainer = ExtensionMaintainer::get()->filter('Email', $author['email'])->first();
			}

			if (!$maintainer && !empty($author['homepage'])) {
				$maintainer = ExtensionMaintainer::get()
					->filter('Name', $author['name'])
					->filter('Homepage', $author['homepage'])
					->first();
			}

			if (!$maintainer && !empty($author['name'])) {
				$maintainer = ExtensionMaintainer::get()
					->filter('Name', $author['name'])
					->filter('Versions.Extension.Name', $package->getName())
					->first();
			}

			if (!$maintainer) {
				$maintainer = new ExtensionMaintainer();
			}

			if(isset($author['name'])) $maintainer->Name = $author['name'];
			if(isset($author['email'])) $maintainer->Email = $author['email'];
			if(isset($author['homepage'])) $maintainer->Homepage = $author['homepage'];
			if(isset($author['role'])) $maintainer->Role = $author['role'];

			$maintainer->write();
			$version->Maintainers()->add($maintainer);
		}
	}

}
