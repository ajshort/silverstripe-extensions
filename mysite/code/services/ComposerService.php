<?php

use Composer\Factory;
use Composer\IO\NullIO;
use Composer\Package\Loader\ArrayLoader;
use Composer\Repository\ArrayRepository;

/**
 * Retrieves packages from Composer.
 */
class ComposerService {

	private $composer;

	public function __construct() {
		$this->composer = Factory::create(new NullIO());
	}

	/**
	 * @return Composer\Composer
	 */
	public function getComposer() {
		return $this->composer;
	}

	/**
	 * Gets all available extension packages.
	 *
	 * @return Composer\Package\PackageInterface[]
	 */
	public function getPackages() {
		$packages = array();
		$loader = new ArrayLoader();

		foreach ($this->composer->getRepositoryManager()->getRepositories() as $repository) {
			foreach ($repository->getMinimalPackages() as $package) {
				if (ExtensionPackage::is_extension_package_type($package['raw']['type'])) {
					$packages[] = $loader->load($package['raw']);
				}
			}
		}

		return $packages;
	}

	/**
	 * Gets all extension packages, grouped into repositories by name.
	 *
	 * @return Composer\Repository\ArrayRepository[]
	 */
	public function getGroupedPackages() {
		$result = array();
		$packages = $this->getPackages();

		foreach ($packages as $package) {
			$name = $package->getName();

			if (!isset($result[$name])) {
				$result[$name] = new ArrayRepository(array($package));
			} else {
				$result[$name]->addPackage($package);
			}
		}

		return $result;
	}

	/**
	 * @param $name
	 * @return Composer\Package\PackageInterface[]
	 */
	public function getGroupedPackage($name) {
		$result = array();
		$loader = new ArrayLoader();

		foreach ($this->composer->getRepositoryManager()->getRepositories() as $repository) {
			foreach ($repository->getMinimalPackages() as $package) {
				if ($package['name'] == $name) {
					$result[] = $loader->load($package['raw']);
				}
			}
		}

		return $result;
	}

}
