<?php
/**
 * The home page controller.
 */
class HomeController extends SiteController {

	public function index() {
		return $this->renderWith(array('Home', 'Page'));
	}

	public function Title() {
		return 'Home';
	}

	public function Link() {
		return Director::baseURL();
	}

	public function PopularExtensions($limit = 10) {
		return ExtensionPackage::get()->sort('Downloads', 'DESC')->limit($limit);
	}

	public function NewestExtensions($limit = 10) {
		return ExtensionPackage::get()->sort('ReleasedAt', 'DESC')->limit($limit);
	}

	public function RandomExtensions($limit = 10) {
		return ExtensionPackage::get()->sort(DB::getConn()->random())->limit($limit);
	}

}
