<?php
/**
 * Displays an individual extensions details.
 */
class ExtensionController extends SiteController {

	public static $allowed_actions = array(
		'index'
	);

	protected $parent;
	protected $extension;

	public function __construct(Controller $parent, ExtensionPackage $extension) {
		$this->parent = $parent;
		$this->extension = $extension;

		parent::__construct();
	}

	public function index() {
		return $this->renderWith(array('Extension', 'Page'));
	}

	public function getRootController() {
		return $this->parent->getRootController();
	}

	public function Extension() {
		return $this->extension;
	}

	public function Title() {
		return $this->extension->Name;
	}

	public function Link() {
		return Controller::join_links($this->parent->Link(), $this->extension->Name);
	}

}
