<?php

use Elastica\Query;
use Elastica\Query\QueryString;
use SilverStripe\Elastica\ElasticaService;

/**
 * Lists extensions and allows searching.
 */
class ExtensionsController extends SiteController {

	public static $url_handlers = array(
		'$Vendor!/$Extension!' => 'extension'
	);

	public static $allowed_actions = array(
		'index',
		'extension'
	);

	public static $dependencies = array(
		'ElasticaService' => '%$SilverStripe\\Elastica\\ElasticaService'
	);

	private $sorts = array(
		'name'    => array('Name', 'ASC'),
		'popular' => array('Downloads', 'DESC'),
		'newest'  => array('ReleasedAt', 'DESC')
	);

	/**
	 * @var \SilverStripe\Elastica\ElasticaService
	 */
	private $elastica;

	public function index() {
		return $this->renderWith(array('Extensions', 'Page'));
	}

	public function extension(SS_HTTPRequest $request) {
		$vendor = $request->param('Vendor');
		$name = $request->param('Extension');
		$ext = ExtensionPackage::get()->filter('Name', "$vendor/$name")->first();

		if (!$ext) {
			$this->httpError(404);
		}

		return new ExtensionController($this, $ext);
	}

	public function setElasticaService(ElasticaService $elastica) {
		$this->elastica = $elastica;
	}

	public function Extensions() {
		$list = ExtensionPackage::get();
		$query = $this->getSearch();
		$sort = $this->getSort();

		if ($query) {
			$query = new Query(new QueryString($query));
			$query->setLimit(count($list));

			$list = $list->byIDs($this->elastica->search($query)->column('ID'));
		}

		if ($sort) {
			$sort = $this->sorts[$sort];
			$list = $list->sort($sort[0], $sort[1]);
		}

		if (!$query && !$sort) {
			$list = $list->sort('Downloads', 'DESC');
		}

		$list = new PaginatedList($list, $this->getRequest());
		$list->setPageLength(20);

		return $list;
	}

	public function ExtensionsSearchForm() {
		$fields = new FieldList(array(
			new TextField(
				'Search', 'Search', $this->getSearch()
			),
			$sort = new DropdownField(
				'Sort',
				'Sort By',
				array(
					'name' => 'Name',
					'popular' => 'Most Popular',
					'newest' => 'Newest'
				),
				$this->getSort()
			)
		));

		$sort->setHasEmptyDefault(true);

		$form = new Form($this, 'ExtensionsSearchForm', $fields, new FieldList());
		$form->setFormMethod('GET');
		$form->setFormAction($this->Link());

		return $form;
	}

	public function Title() {
		return 'Extensions';
	}

	public function Link() {
		return Controller::join_links(Director::baseURL(), 'extensions');
	}

	private function getSearch() {
		return $this->getRequest()->getVar('Search');
	}

	private function getSort() {
		$sort = $this->getRequest()->getVar('Sort');

		if($sort && array_key_exists($sort, $this->sorts)) {
			return $sort;
		}
	}

}
