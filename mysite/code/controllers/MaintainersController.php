<?php
/**
 * Handles displaying package maintainers.
 */
class MaintainersController extends SiteController {

	public static $url_handlers = array(
		'$MaintainerID!' => 'maintainer'
	);

	public static $allowed_actions = array(
		'index',
		'maintainer'
	);

	public function index() {
		return $this->renderWith(array('Maintainers', 'Page'));
	}

	public function maintainer($request) {
		$id = $request->param('MaintainerID');
		$maintainer = ExtensionMaintainer::get()->byID($id);

		if (!$maintainer) {
			$this->httpError(404);
		}

		return new MaintainerController($this, $maintainer);
	}

	public function Title() {
		return 'Maintainers';
	}

	public function Link() {
		return Controller::join_links(Director::baseURL(), 'maintainers');
	}

	public function Maintainers() {
		return ExtensionMaintainer::get();
	}

}
