<?php
/**
 * Displays an individual maintainer.
 */
class MaintainerController extends SiteController {

	public static $allowed_actions = array(
		'index'
	);

	protected $parent;
	protected $maintainer;

	public function __construct(Controller $parent, ExtensionMaintainer $maintainer) {
		$this->parent = $parent;
		$this->maintainer = $maintainer;

		parent::__construct();
	}

	public function index() {
		return $this->renderWith(array('Maintainer', 'Page'));
	}

	public function getRootController() {
		return $this->parent->getRootController();
	}

	public function Title() {
		return $this->maintainer->Name;
	}

	public function Link() {
		return Controller::join_links($this->parent->Link(), $this->extension->Name);
	}

	public function Maintainer() {
		return $this->maintainer;
	}

}
