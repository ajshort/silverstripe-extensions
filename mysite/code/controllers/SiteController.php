<?php
/**
 * The project base controller class.
 */
abstract class SiteController extends Controller {

	public function getRootController() {
		return $this;
	}

	public function Menu() {
		$controllers = array('HomeController', 'ExtensionsController', 'MaintainersController');
		$menu = new ArrayList();
		$root = Controller::curr()->getRootController();

		foreach ($controllers as $class) {
			$menu->push(new ArrayData(array(
				'Title' => singleton($class)->Title(),
				'Link' => singleton($class)->Link(),
				'Active' => $root instanceof $class
			)));
		}

		return $menu;
	}

	public function ExtensionsSearchForm() {
		return singleton('ExtensionsController')->ExtensionsSearchForm();
	}

}
