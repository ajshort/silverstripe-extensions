<?php
/**
 * An individual version for an {@link ExtensionPackage}.
 */
class ExtensionVersion extends DataObject {

	public static $db = array(
		'Name'               => 'Varchar(255)',
		'Type'               => 'Varchar(50)',
		'Description'        => 'Text',
		'Version'            => 'Varchar(50)',
		'PrettyVersion'      => 'Varchar(50)',
		'VersionAlias'       => 'Varchar(50)',
		'PrettyVersionAlias' => 'Varchar(50)',
		'IsDevelopment'       => 'Boolean',
		'Source'              => 'MultiValueField',
		'Dist'                => 'MultiValueField',
		'Homepage'            => 'Varchar(255)',
		'License'             => 'MultiValueField',
		'Support'             => 'MultiValueField',
		'Extra'               => 'MultiValueField',
		'ReleasedAt'          => 'SS_Datetime',
		'UpdatedAt'           => 'SS_Datetime'
	);

	public static $has_one = array(
		'Extension' => 'ExtensionPackage'
	);

	public static $has_many = array(
		'Links' => 'ExtensionLink'
	);

	public static $many_many = array(
		'Maintainers' => 'ExtensionMaintainer'
	);

	public static $default_sort = '"ID" DESC';

	public function getDisplayVersion() {
		return $this->PrettyVersionAlias ?: $this->PrettyVersion;
	}

}
