<?php

use Elastica\Document;
use Elastica\Type\Mapping;

/**
 * An extension package, which can have several versions.
 */
class ExtensionPackage extends DataObject {

	const TYPE_MODULE = 'silverstripe-module';
	const TYPE_THEME  = 'silverstripe-theme';

	public static $db = array(
		'Name'        => 'Varchar(255)',
		'Type'        => 'Varchar(50)',
		'Description' => 'Text',
		'Readme'      => 'HTMLText',
		'Tags'        => 'MultiValueField',
		'Repository'  => 'Varchar(255)',
		'Downloads'   => 'Int',
		'ReleasedAt'  => 'SS_Datetime',
		'UpdatedAt'   => 'SS_Datetime',
		'BuiltAt'     => 'SS_Datetime'
	);

	public static $has_one = array(
		'Vendor' => 'ExtensionVendor'
	);

	public static $has_many = array(
		'Versions' => 'ExtensionVersion'
	);

	public static $searchable_fields = array(
		'Name',
		'Description'
	);

	public static $extensions = array(
		'SilverStripe\\Elastica\\Searchable'
	);

	public static function is_extension_package_type($type) {
		return $type == self::TYPE_MODULE || $type == self::TYPE_THEME;
	}

	public function getVersion($version) {
		return $this->Versions()->filter('Version', $version)->first();
	}

	public function getMaintainers() {
		return $this->Versions()->relation('Maintainers');
	}

	public function getElasticaMapping() {
		return new Mapping(null, array(
			'name'        => array('type' => 'string'),
			'description' => array('type' => 'string'),
			'type'        => array('type' => 'string'),
			'vendor'      => array('type' => 'string'),
			'tags'        => array('type' => 'string', 'index_name' => 'tag')
		));
	}

	public function getElasticaDocument() {
		return new Document($this->ID, array(
			'name'        => $this->Name,
			'description' => $this->Description,
			'type'        => str_replace('silverstripe-', '', $this->Type),
			'vendor'      => $this->VendorName(),
			'tags'        => $this->Tags->getValue(),
			'_boost'      => sqrt($this->Downloads)
		));
	}

	public function PackageName() {
		return substr($this->Name, strpos($this->Name, '/') + 1);
	}

	public function VendorName() {
		return substr($this->Name, 0, strpos($this->Name, '/'));
	}

	public function VendorLink() {
		$query = http_build_query(array(
			'Search' => 'vendor:"' . $this->VendorName() . '"',
			'Sort'   => 'popular'
		));

		return Controller::join_links(
			singleton('ExtensionsController')->Link(), "?$query"
		);
	}

	public function Link() {
		return Controller::join_links(
			singleton('ExtensionsController')->Link(), $this->Name
		);
	}

}
