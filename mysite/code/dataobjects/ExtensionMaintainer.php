<?php
/**
 * A maintainer for an extension, can have several extensions.
 */
class ExtensionMaintainer extends DataObject {

	public static $db =  array(
		'Name'     => 'Varchar(255)',
		'Email'    => 'Varchar(100)',
		'Homepage' => 'Varchar(100)',
		'Role'     => 'Varchar(100)'
	);

	public static $belongs_many_many = array(
		'Versions' => 'ExtensionVersion'
	);

	public static $default_sort = '"Name"';

	protected function onBeforeWrite() {
		if ($this->Homepage && strpos($this->Homepage, '//') === false) {
			$this->Homepage = 'http://' . $this->Homepage;
		}

		parent::onBeforeWrite();
	}

	public function getExtensions() {
		return ExtensionPackage::get()->filter('ID', $this->Versions()->column('ExtensionID'));
	}

	public function GravatarUrl($size, $default = 'mm') {
		return sprintf(
			'http://www.gravatar.com/avatar/%s?s=%d&d=%s',
			md5(strtolower(trim($this->Email))),
			$size,
			$default
		);
	}

	public function Link() {
		return Controller::join_links(
			Director::baseURL(), 'maintainers', $this->ID
		);
	}

}
